package com.example.practica0193java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnSaludar, btnLimpiar, btnCerrar;
    private TextView lblSaludar;
    private EditText txtSaludo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Relación de los objetos de java con las views xml
        btnSaludar = (Button) findViewById(R.id.btnSaludar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);
        txtSaludo = (EditText) findViewById(R.id.txtSaludo);

        //Codificar el evento click del boton Saludar
        btnSaludar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //Aqui se realiza la programacion
                if (txtSaludo.getText().toString().matches("")){
                    //Falto capturar nombre
                    Toast.makeText(MainActivity.this, "Favor de ingresar el nombre", Toast.LENGTH_SHORT).show();
                }
                else {
                    String saludar = txtSaludo.getText().toString();
                    lblSaludar.setText("Hola " + saludar + " Como estas?");
                }
            }
        });
        //Codificar el evento click del boton limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //Aqui se realiza la programacion
                lblSaludar.setText("");
                txtSaludo.setText("");
                txtSaludo.requestFocus();
            }
        });
        //Codificar el evento click del boton limpiar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {  //Aqui se realiza la programacion
                finish();
            }
        });
    }
}